package lms;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }


    /**
     * Simple test sending 10 calls to {@link Dispatcher}.
     */
    public void testTenCalls() {
        final Dispatcher dispatcher = new DispatcherImpl(1, 1, 1, 10);
        assertFalse(dispatcher.isWorking());
        for (int i = 0; i < 10; i++) {
            dispatcher.dispatchCall(new Call(i));
        }
        while (dispatcher.isWorking());
        assertEquals(0, dispatcher.getRejections());
    }

    /**
     * Testing {@link Call} initial state and process through the call. Testing {@link Dispatcher} priority queue for
     * employees, rejections system for calls.
     */
    public void testDispatcher() {
        final Call call = new Call(1);
        assertNull(call.getHandler());
        assertFalse(call.isAlive());
        final Dispatcher dispatcher = new DispatcherImpl(1, 1, 1, 7);
        assertFalse(dispatcher.isWorking());
        dispatcher.dispatchCall(call);
        assertTrue(dispatcher.isWorking());
        assertNotNull(call.getHandler());
        assertEquals(1, call.getHandler().getRole());
        assertEquals(0, dispatcher.getRejections());
        final Call supervisorCall = new Call(2);
        dispatcher.dispatchCall(supervisorCall);
        assertNotNull(supervisorCall.getHandler());
        assertEquals(2, supervisorCall.getHandler().getRole());
        final Call directorCall = new Call(3);
        dispatcher.dispatchCall(directorCall);
        assertNotNull(directorCall.getHandler());
        assertEquals(3, directorCall.getHandler().getRole());
        while(dispatcher.isWorking());
        assertFalse(dispatcher.isWorking());
        assertEquals(0, dispatcher.getRejections());
    }

    /**
     * Test for asserting that when all employees are busy, the calls would be enqueued. When queue limit is reached,
     * calls would be rejected.
     */
    public void testCallsQueue() {
        final Dispatcher dispatcher = new DispatcherImpl(1, 1, 1, 7);
        for (int i = 0; i < 10; i++) {
            dispatcher.dispatchCall(new Call(i));
        }
        assertEquals(0, dispatcher.getRejections());
        assertTrue(dispatcher.isWorking());
        dispatcher.dispatchCall(new Call(10));
        assertEquals(1, dispatcher.getRejections());
        while(dispatcher.isWorking());
        assertEquals(1, dispatcher.getRejections());
    }

    /**
     * Producing concurrent calls to Dispatcher.
     * @throws InterruptedException
     */
    public void testProducers() throws InterruptedException {
        final Dispatcher dispatcher = new DispatcherImpl(2, 1, 1, 6);
        final Producer prod1 = new Producer(5, 10, dispatcher);
        final Producer prod2 = new Producer(6, 20, dispatcher);
        assertFalse(dispatcher.isWorking());
        prod1.start();
        prod2.start();
        Thread.sleep(100);
        assertTrue(dispatcher.isWorking());
        while (dispatcher.isWorking());
        assertFalse(dispatcher.isWorking());
        assertEquals(1, dispatcher.getRejections());
    }
}
