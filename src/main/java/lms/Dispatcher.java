package lms;

import java.util.PriorityQueue;

public interface Dispatcher {
    /**
     * Receive a {@link Call} and dispatch it to the corresponding {@link Employee}.
     * @param call The call to be dispatched.
     */
    void dispatchCall(Call call);

    /**
     * Add an employee to the queue in order to be available to handle calls.
     * @param employee the employee to be enqueued.
     */
    void enqueueEmployee(Employee employee);

    /**
     * @return {@code true} when at least a call is being handled or in the queue, and {@code false} in any other case.
     */
    boolean isWorking();

    /**
     * @return the amount of total rejections. A rejection would be produced in case that the calls' queue limit is
     * reached, and a new call is received for dispatching.
     */
    int getRejections();
}
