package lms;

import java.util.Random;

public class Call extends Thread{

    private final int duration;
    private Employee handler;

    /**
     * Set the {@link Thread} name using the id provided in constructor. Define the random duration of the Call.
     * @param id used for call identification
     */
    public Call(int id) {
        setName(Integer.toString(id));
        this.duration = new Random().nextInt((10000 - 5000) + 1) + 5000;
    }

    /**
     * block the Thread while the call is being processed by the handler. When the call ends, notify the handler.
     */
    @Override
    public void run() {
        try {
            Thread.sleep(duration);
            System.out.println("Call " + getName() + " ended after " + duration + "ms.");
            handler.endCall();
        } catch (InterruptedException e) {
            System.out.println("Thread " + getName() + " interrupted.");
            e.printStackTrace();
        }
    }

    /**
     * Define the {@link Employee} that would handle the Call, in order to be notified when the call ends.
     * @param handler the employee that handles the call.
     */
    public void setHandler(Employee handler) {
        this.handler = handler;
    }

    /**
     * Retrieve the current {@link Employee} assigned to the call. (Used for testing).
     * @return the {@link Employee} previously set to handle the call or {@code null} if the handler is not defined.
     */
    public Employee getHandler() {
        return handler;
    }
}
