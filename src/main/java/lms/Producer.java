package lms;

/**
 * Class for sending calls to a {@link Dispatcher} using a different thread.
 */
public class Producer extends Thread{

    private final int calls;
    private final int code;
    private final Dispatcher dispatcher;

    public Producer(int calls, int code, Dispatcher dispatcher) {
        this.calls = calls;
        this.code = code;
        this.dispatcher = dispatcher;
    }

    @Override
    public void run() {
        for (int i = 0; i < calls; i++) {
            dispatcher.dispatchCall(new Call(code + i));
        }
    }
}
