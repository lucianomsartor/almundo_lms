package lms;

import java.util.*;

/**
 * Implementation of the interface {@link Dispatcher}. Provides a {@link PriorityQueue} for enqueuing employees.
 * A queue limit is defined in order to determine when calls should be enqueued or rejected.
 */
public class DispatcherImpl implements Dispatcher {
    private final Queue<Call> calls;

    private final Queue<Employee> employees;
    private final int queueLimit;
    private final int employeesCount;
    private int rejections;


    public DispatcherImpl(int maxOperators, int maxSupervisors, int maxDirectors, int queueLimit) {
        this.queueLimit = queueLimit;
        this.employeesCount = maxDirectors + maxOperators + maxSupervisors;
        calls = new LinkedList<Call>();
        employees = new PriorityQueue<Employee>();
        employees.addAll(setEmployeeList(maxOperators, "Op", 1));
        employees.addAll(setEmployeeList(maxSupervisors, "Sup", 2));
        employees.addAll(setEmployeeList(maxDirectors, "Dir", 3));
    }

    private List<Employee> setEmployeeList(int size, String name, int role) {
        final List<Employee> employees = new ArrayList<Employee>();
        while (employees.size() < size) {
            employees.add(new Employee(this, name + "-" + employees.size(), role));
        }
        return employees;
    }


    public synchronized void dispatchCall(Call call) {
        if (calls.size() >= queueLimit) {
            System.out.println("Waiting list is full! Call " + call.getName() + " is rejected.");
            rejections++;
            return;
        }
        System.out.println("Call " + call.getName() + " received.");
        assignCall(call);
    }

    private void assignCall(Call call) {
        if (employees.isEmpty()) {
            calls.add(call);
            System.out.println("Call " + call.getName() + " added to waiting list.");
        } else {
            employees.poll().consumeCall(call);
        }
    }

    public synchronized void enqueueEmployee(Employee employee) {
        employees.add(employee);
        if (!calls.isEmpty()) {
            assignCall(calls.poll());
        }
    }

    public synchronized boolean isWorking() {
        return !calls.isEmpty() || employees.size() < employeesCount;
    }

    public int getRejections() {
        return rejections;
    }
}
