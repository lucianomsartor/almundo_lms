package lms;

import java.util.PriorityQueue;

public class Employee implements Comparable<Employee> {

    private final Dispatcher dispatcher;
    private final String id;
    private int role;

    /**
     * @param dispatcher the {@link Dispatcher} in which the employee is included to handle calls.
     * @param id employee's identifier
     * @param role whether the employee is an operator, supervisor or director.
     */
    public Employee(Dispatcher dispatcher, String id, int role) {
        this.dispatcher = dispatcher;
        this.id = id;
        this.role = role;
    }

    /**
     * Method that assigns the current employee as handler to the call and starts the processing of the call.
     * @param call the {@link Call} to be processed.
     */
    public void consumeCall(Call call) {
        System.out.println("Employee " + id + " received call " + call.getName());
        call.setHandler(this);
        call.start();
    }

    /**
     * Set the employee as available for taking calls.
     */
    public void endCall() {
        dispatcher.enqueueEmployee(this);
    }

    /**
     * Comparator used for the {@link PriorityQueue}
     * @param employee the second employee to be compared with.
     * @return the value of comparision between the employees roles.
     */
    public int compareTo(Employee employee) {
        if (this.getRole() > employee.getRole()) {
            return 1;
        } else if (this.getRole() < employee.getRole()) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * @return the role defining if an Employee is an operator, supervisor or director.
     */
    public int getRole() {
        return this.role;
    }
}
